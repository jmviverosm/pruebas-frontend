import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../../service/users/users.service';
import { ResultDataUser } from '../../interfaces/interfaces';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.scss']
})
export class ListaComponent implements OnInit {

  listUsers: ResultDataUser;
  constructor(
    private router: Router,
    public usersService: UsersService,
  ) { }

  ngOnInit() {
    this.getListUsers();
  }

  /**
   * @description metodo que llama al servicio de obtener listado de usuarios
   */
  getListUsers() {
    this.usersService.getUsers()
      .subscribe(response => {
        // console.log(response);
        if (response._meta.success) {
          this.listUsers = response.result;
        }
      }, err => {
        console.log(err);
      }, () => {
      });
  }

  /**
   * @description metodo que obtiene el id del usuario y lo redirige a la vista de detalle de usuario
   * @param {string} id id del usuario por el cual se va a consultar la información
   */
  getUserDetail(id: string){
    this.router.navigate(["/perfil", id]);
  }
}
