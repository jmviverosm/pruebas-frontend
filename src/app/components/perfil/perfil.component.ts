import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UsersService } from '../../service/users/users.service';
import { ResultDataUser } from '../../interfaces/interfaces';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent implements OnInit {

  nameUser: string = '';
  detailUser: ResultDataUser;
  itemsAsignados: any = [
    {
      name: 'Proyectos',
      totalItems: 1,
      estado: {
        name: 'Archivados',
        total: 0
      }
    },
    {
      name: 'Tareas',
      totalItems: 2,
      estado: {
        name: 'Archivados',
        total: 0
      }
    },
    {
      name: 'Hitos',
      totalItems: 0,
      estado: {
        name: 'Archivados',
        total: 0
      }
    }
  ];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public usersService: UsersService,
  ) { }

  ngOnInit() {
    this.getDataParams();
  }

  /**
   * @description funcion que obtiene los parametros que lleguen por url por medio del activatedRoute de Angular
   */
  getDataParams(){
    this.activatedRoute.paramMap.subscribe(params => {
      if(params.has('id')){
        // llamo al servicio que trae el detalle del usuario
        this.getUserDetail(params.get('id'));
      }
    });
  }

  /**
   * @description metodo que llama al servicio de obtener detalle de usuario por id
   * @param {string} id es el id del usuario por el cual se vana traer los datos, en realidad es un number
   * pero como llega por url y el servicio es get se envia como string
   */
  getUserDetail(id: string) {
    this.usersService.getUserProfileData(id)
      .subscribe(response => {
        if (response._meta.success) {
          this.nameUser = `${response.result.first_name} ${response.result.last_name}`;
          // this.detailUser = response.result;
        }
      }, err => {
        console.log(err);
      }, () => {
      });
  }
}
