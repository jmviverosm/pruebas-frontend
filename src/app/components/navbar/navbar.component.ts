import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  navigationMenu: any = [
    {
      name: 'Inicio',
      link: '**'
    },
    {
      name: 'Proyectos',
      link: '**'
    },
    {
      name: 'Todo',
      link: '**'
    },
    {
      name: 'Calendario',
      link: '**'
    },
    {
      name: 'Estados',
      link: '**'
    },
    {
      name: 'Personas',
      link: 'lista'
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}
