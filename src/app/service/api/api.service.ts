import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { environment } from '../../../environments/environment';
import { ServicesUserResponse } from '../../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
/**
 * @description clase que tiene los principales metodos como put, post, get
 */
export class ApiService {

  constructor(
    public http: HttpClient,

  ) { }

  /**
   *
   * @param {string} path url con datos por get del servicio a llamar
   */
  get(path: string) {
    return this.http.get<ServicesUserResponse>(environment.apiUrl + path);
  }

  /**
   *
   * @param {string} path url del servicio a llamar
   * @param {any} data datos a enviar al servicio en formato JSON
   */
  post(path: string, data: any) {
    return this.http.post(environment.apiUrl + path, data);
  }
}
