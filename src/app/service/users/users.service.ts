import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    public Api: ApiService
  ) { }

  /**
   * @description servicio que devuelve el listado de usuarios
   */
  getUsers() {
    return this.Api.get('/users?_format=json&access-token=2sTTRZ41l-OXUyHqJQDmVQph7HYgT8A0Mw9X');
  };

  /**
   * @description servicio que devuelve el detalle de un usuario por id
   * @param {number} id ID del usuario del cual se van a traer los datos
   */
  getUserProfileData(id: string) {
    return this.Api.get(`/users/${id}?_format=json&access-token=2sTTRZ41l-OXUyHqJQDmVQph7HYgT8A0Mw9X`);
  }
}
