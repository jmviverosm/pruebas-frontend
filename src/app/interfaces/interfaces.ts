export interface ServicesUserResponse {
  _meta: Meta;
  result: ResultDataUser;
}

export interface ResultDataUser {
  id: string;
  first_name: string;
  last_name: string;
  gender: string;
  dob: string;
  email: string;
  phone: string;
  website: string;
  address: string;
  status: string;
  _links: Links;
}

export interface Links {
  self: Self;
  edit: Self;
  avatar: Self;
}

export interface Self {
  href: string;
}

export interface Meta {
  success: boolean;
  code: number;
  message: string;
  totalCount: number;
  pageCount: number;
  currentPage: number;
  perPage: number;
  rateLimit: RateLimit;
}

export interface RateLimit {
  limit: number;
  remaining: number;
  reset: number;
}
